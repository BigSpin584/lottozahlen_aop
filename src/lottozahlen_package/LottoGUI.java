package lottozahlen_package;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;


public class LottoGUI extends JFrame {
	private JPanel mainPanel = new JPanel(new GridLayout(2,1));
	
	private String[] header = {"KW", "1.Zahl", "2.Zahl","3.Zahl","4.Zahl","5.Zahl","6.Zahl"};
	private Integer[][] daten =  new Integer[52][7];; //Datenmatrix
	private JTable  tabelle = new JTable (daten,header);
	private JPanel p1 = new JPanel(new BorderLayout());
	private JPanel p2 = new JPanel(null);
	private JTextField number = new JTextField();
	private JButton genBtn = new JButton("Generieren");
	private JButton saveBtn = new JButton("Speichern");
	
	
	public LottoGUI() {
		
		this.setLocation(100,100);
		this.setSize(800,500);
		this.setTitle("Lottozahlen");
		
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		JMenu dateiMenu = new JMenu("Datei");
		menuBar.add(dateiMenu);
		JMenuItem saveItem = new JMenuItem("Speichern");
		dateiMenu.add(saveItem);
		JMenuItem exitItem = new JMenuItem("Beenden");
		dateiMenu.add(exitItem);
		
		createP1();
		mainPanel.add(p1);
		createP2();
		mainPanel.add(p2);
		
		
		setContentPane(mainPanel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void createP1() {
		p1.setLayout(new BorderLayout());
		p1.add(new JScrollPane(tabelle), BorderLayout.CENTER);
	}
	
	private void createP2() {
		JLabel woche = new JLabel("Kalenderwoche");
		p2.add(woche);
		woche.setBounds(30,20,150,20);
		p2.add(number);
		number.setBounds(180,20,150,20);
		p2.add(genBtn);
		genBtn.setBounds(180,50,150,20);
		p2.add(saveBtn);
		saveBtn.setBounds(350,50,150,20);
	}
	
	
	
	
	
	
	
	
	
}
