package lottozahlen_package;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LottoDaten {
	private static LottoDaten ref = null;
	public static LottoDaten getDaten() {
		return (ref == null) ? ref = new LottoDaten() : ref;
		
		
	}
	private  LottoDaten() {	
		daten = new HashMap<Integer, Set<Integer>>();
	}
	private Map<Integer, Set<Integer>> daten;
	/*public Set<Integer> getTipp(Integer w) {
		Hat er nicht so Bock gehabt fertig zu machen
	}*/
	public void genTipp(Integer w) throws IllegalArgumentException {
		if(w < 1 || w > 52) throw new IllegalArgumentException("kein gültiger Wert!");
			Set<Integer> tipp = new HashSet<Integer>();
		while (tipp.size() < 6) {
			tipp.add(Main.rand.nextInt(49) + 1);
		}
		daten.put(w, tipp);
	}
}
